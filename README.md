# Dyadic


## Fork Details

 * Dyadic is a fork of [Atomic](https://github.com/indrora/Atomic);
 * Atomic is a fork of [YAAIC](https://github.com/pocmo/Yaai).


## Dependencies

 * [PircBot IRC API](http://www.jibble.org/pircbot.php) under GNU General Public License
 * [ViewPagerIndicator](http://viewpagerindicator.com) under Apache license 2.0
 * [ActionBarSherlock](http://actionbarsherlock.com) under Apache license 2.0
 * [MemorizingTrustManager](https://github.com/ge0rg/MemorizingTrustManager) under Apache license 2.0


## Download

You can download latest artifact from Gitlab CI via [this link](https://gitlab.com/iria_somobu/dyadic/-/jobs/artifacts/master/file/dyadic.apk?job=build).


## License (GPLv3)

```
Copyright 2009 - 2013 Sebastian Kaspari
Copyright 2014 - 2016 Morgan `Indrora' Gangwere
Copyright 2022 - 2022 Iria Somobu

Dyadic is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Dyadic is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this software. If not, see http://www.gnu.org/licenses/
```

