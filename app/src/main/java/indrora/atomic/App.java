package indrora.atomic;

import android.app.Application;
import android.content.Context;
import android.content.res.Resources;

import indrora.atomic.model.ColorScheme;
import indrora.atomic.model.Settings;

public class App extends Application {

    private static Settings _s;

    private static Context _ctx;

    public static Context getAppContext() {
        return _ctx;
    }

    public static ColorScheme getColorScheme() {
        return new ColorScheme(_s.getColorScheme(), _s.getUseDarkColors());
    }


    public static Settings getSettings() {
        if (_s == null) {
            _s = new Settings(getAppContext());
        }
        return _s;
    }

    private static Resources _r;

    public static Resources getSResources() {
        return _r;
    }


    @Override
    public void onCreate() {
        _ctx = getApplicationContext();

        Atomic.getInstance().loadServers(_ctx);

        indrora.atomic.model.Settings _settings = new Settings(this);
        _s = _settings;

        // Release 16 changes things for colors.
        // This is a much more elegant solution than I had here. Be glad.
        if (_s.getLastRunVersion() < 16) {
            _settings.setColorScheme("default");
        }

        _r = getResources();

        super.onCreate();
    }
}
